%-------------------------------------------------------------
%Program:   Critical Pairs
%Authors:   Héctor Sánchez, Christian Arzate, César Montiel
%Date:      September 2014
%-------------------------------------------------------------
%Examples
%unify_pair(e+X->X,A,Y+f->Y,B).
%unify_pair(e+X,1,Y+e,1).
%unify_pair(0+X,X,Y+Y,0).
%unify_pair(e+X,X,Y+f,Y).
%unify_pair(0+X,X,((-Y)+Y),0).
%unify_pair(0+X,X,Y+Y,0).
%unify_pair(X+X,X,(X+Y)+Z,X+(Y+Z)).
%unify_pair(0+X,X,(X+Y)+Z,X+(Y+Z)).
%-------------------------------------------------------------
%Declarations
:- module(critical_pairs, [unify_pair/4]).
:- [cnf,sound_unify].
%-------------------------------------------------------------
%Functions

%Unifies a pair of expressions
% L1 -> R1
% L2 -> R2
% Calculates the unificator term Phi
unify_pair(L1,R1,L2,R2):-
    unify(L1,L2),
    print('{'),print(L1),print(','),print(R1),print('}').


%--unify en su implementación de sound_unify puede recibir listas y unifica todos los términos

%-------------------------------------------------------------
%Legacy
%receive_pair(A,B,X,Y):-
%    cnf(A,X),
%    cnf(B,Y).%,
%   unify(X,Y).
%unify_expressions(A,B):-
%    unify(A,B).
%
%replace_list_elements(_, _, [], []).
%replace_list_elements(O, R, [O|T], [R|T2]) :-
%replace_list_elements(O, R, T, T2).
%replace_list_elements(O, R, [H|T], [H|T2]) :-
%H \= O,
%replace_list_elements(O, R, T, T2).
%-------------------------------------------------------------