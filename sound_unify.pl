/*
program:        Unification with occurs check - modified from the Sterling 
                and Shapiro algorithm.
file:		~asch/programs/prolog/Lib/sound_unify.pl
author:         Andreas Schoter <asch@aisb.ed.ac.uk>
date:           Mon Oct  2, 1995
last update:	Tue Oct  3, 1995
*/
/* PREDICATES DEFINED IN THIS FILE

not_occurs_in/2               unify/2
not_occurs_in_args/2          unify/3
unify/1                       unify_args/2
*/

:- module(sound_unify,['==='/2,unify/1,unify/2]).

:- op(700,xfx,===).


/*	===/2
mode:	+Term1 === +Term2
read:	Unifies Term1 with Term2 with an occurs check.  Top-level interface
     to unify/2 predicate.
*/

X === Y:- unify(X,Y).


/*	unify/1
mode:	(+TermList)
read:	Unify every term in the TermList.
*/

unify([A,B|ExprList]):-
	unify(ExprList,A,B).


/*	unify/3
mode:	(+TermList, +Term1, +Term2)
read:	Unify Term1 and Term2, then recursively traverse the TermList.
*/

unify([],A,B):- 
	unify(A,B).

unify([A|Rest],B,C):-
	unify(B,C),
	unify(Rest,A,B).


/*	unify/2
mode:	(+Term1,+Term2)
read:	Unifies Term1 with Term2 with an occurs check.
*/

% both terms are variables and they unify

unify(X,Y):-
	var(X),var(Y),!,
	X=Y.

% the first term is a variable, the second is not, the first term does
% not occur in the second, and they unify

unify(X,Y):-
	var(X),
%	nonvar(Y),
	!,not_occurs_in(X,Y),
	X=Y.

% the second term is a variable, the first is not, the second term does
% not occur in the first, and they unify

unify(X,Y):-
	var(Y),
%	nonvar(X),
	!,not_occurs_in(Y,X),
	X=Y.

% both terms are atomic and they are identical

unify(X,Y):-
%	nonvar(X),
%	nonvar(Y),
	atomic(X),
	atomic(Y),!,
	X==Y.

% both terms are compound objects - decompose them: they unify if they
% have the same principle functor and arity and all of their arguments
% unify with an occurs check

unify(X,Y):-
%	nonvar(X),
%	nonvar(Y),
%	compound(X),
%	compound(Y),
	functor(X,F,N),
	functor(Y,F,N),
	X =.. [F|Args1],
	Y =.. [F|Args2],
	unify_args(Args1,Args2).


/*	unify_args/2
mode:	(+ArgList1, +ArgList2)
read:	Unifies each item in ArgList1 with the corresponding item in
     ArgList2 using the occurs check unify/2.
*/

unify_args([],[]).

unify_args([A1|Args1],[A2|Args2]):-
	unify(A1,A2),!,
	unify_args(Args1,Args2).


/*	not_occurs_in/2
mode:	(+Var, +Term)
read:	Ensures that the given Var does not occur in the Term in any way.
*/

not_occurs_in(X,Y):-
	var(Y),!,
	X \== Y.

not_occurs_in(_X,Y):-
	nonvar(Y),
	atomic(Y),!.

not_occurs_in(X,Y):-
	nonvar(Y),
%	compound(Y),
	Y =.. [_|Args],
	not_occurs_in_args(Args,X).


/*	not_occurs_in_args/2
mode:	(+ArgList, +Var)
read:	Ensures that the given Var does not occur in any of the term in
     the ArgList.
*/

not_occurs_in_args([],_).

not_occurs_in_args([A|Args],X):-
	not_occurs_in(X,A),
	not_occurs_in_args(Args,X).


% == END OF FILE ===========================================================
