:-[sound_unify, cnf].

parent(_):-true.
grandparent(X):-parent(parent(X)).
i(_):-true.

/*********************************************************************
*                                 Rules                              *
********************************************************************/

rule(1, e+X, X).
rule(2, Y+f, Y).
rule(3, 0+X, X).
rule(4, ((-Y)+Y), 0).
rule(5, (X+Y)+Z, X+(Y+Z)).
rule(6, X+X, X).
rule(7, parent(parent(Z)), grandparent(Z)).
rule(12, e*X, X).
rule(13, i(X)*X, e).
rule(14, (X*Y)*Z, X*(Y*Z)).

ruleset(1, List):-  List = [[L1, R1], [L2, R2]], rule(1,L1,R1), rule(2, L2, R2).
ruleset(2, List):-  List = [[L3, R3], [L4, R4], [L5, R5]],
                    rule(3,L3,R3), rule(4, L4, R4),rule(5,L5,R5).
ruleset(3, List):-  List = [[L6, R6], [L5, R5]], rule(6,L6,R6), rule(5, L5, R5).
ruleset(4, List):-  List = [[L7, R7]], rule(7,L7,R7).


get_Subterms( Expression, PartialResult, Result):-  atom(Expression),!,
                                                    Result= [Expression|PartialResult].

get_Subterms( Expression, PartialResult, Result):-  var(Expression),!,
                                                    Result= [Expression|PartialResult].

get_Subterms( Expression, PartialResult, Result ):- Expression =.. [_, SubExpA],
                                                    get_Subterms(SubExpA,[],PartResA),
                                                    append(PartResA, [Expression|PartialResult], Result),!.

get_Subterms( Expression, PartialResult, Result ):- !,Expression =.. [_, SubExpA, SubExpB],
                                                    get_Subterms(SubExpA,[],PartResA),
                                                    get_Subterms(SubExpB,[],PartResB),
                                                    append(PartResA, PartResB, PartResC),
                                                    append(PartResC, [Expression|PartialResult], Result).

/*
*       superpose/3 (+N, +M, ?C)
*       Provided N and M, superpose extracts all critical pairs between rules #N and #M
*/
superpose(N, M, C):-    superposeN(N, M, C),
                        portray_clause(C).

superposeN(N, M, CriticalPair):-rule(N,L1,R1),
                                rule(M,L2, R2),
                                get_Subterms(L1,[],Bits),
                                match(Bits,L2,R2),
                                replace_all_B(R2, L2, L1, NewL1),
                                CriticalPair=[R1,NewL1].

replace_all_B(NewTerm, OldTerm, Exp, NewExp) :- findall(Pos, rexp_at(Exp, Pos, OldTerm), PosList),
                                                replace_all_list(PosList, NewTerm, Exp, NewExp).

apply(N, OldExpression, NewExpression):-   rule(N, L, R),
                                            replace_all(R, L, OldExpression, NewExpression).

match([Bit|RemainderBits],TargetL2,_):- RemainderBits=[],
                                        unify_with_occurs_check(Bit,TargetL2).
match([Bit|RemainderBits],TargetL2,_):- RemainderBits\=[],
                                        unify_with_occurs_check(Bit,TargetL2);
                                        match(RemainderBits,TargetL2,_).

% working_directory(_, '/Users/efrenchavezochoa/Desktop/MODELOS/Proloco').
% [filename].
%replaceSonIf(Exp, ToMatch, NewTerm, NewExp):-   Exp=..[Op, Match, B],
%Match==ToMatch,
%NewExp=..[Op, NewTerm, B].
%replaceSonIf(Exp, ToMatch, NewTerm, NewExp):-   Exp=..[Op, Match, B],
%Match==ToMatch,
%NewExp=..[Op, NewTerm, B].

%apply(N, OldExpression, NewExpression):-rule(N, L, R),
%rexp_at(OldExpression, Position, L),
%replace(R, Position, OldExpression, NewExpression).