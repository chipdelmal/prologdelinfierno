%consult(sound_unify).
:-[sound_unify].
/*********************************************************************
 *                          Default Operators                        *
 ********************************************************************/

:- op(600, xfy, <->).
:- op(575, xfy, ->).
:- op(550, xfy, ^).
:- op(525, xfy, v).
:- op(500, fy, ~).


%   Meta-logical Term-Rewriting Operator

:- op(700, xfx, :=>).

parent(_):-true.
grandparent(_):-true.

/*********************************************************************
*                                 Rules                              *
********************************************************************/

rule(1,e+X, X).
rule(2,Y+f, Y).
rule(3,parent(parent(Z)), grandparent(Z)).
rule(4,e+X,1).
rule(12,Y+e,1).
rule(5,0+X,X).
rule(7,((-Y)+Y),0).
rule(9,Y+Y,0).
rule(10,X+X,X).
rule(11,(X+Y)+Z,X+(Y+Z)).


get_Subterms( Expression, PartialResult, Result):-  atom(Expression),!,
                                                    Result= [Expression|PartialResult].

get_Subterms( Expression, PartialResult, Result):-  var(Expression),!,
                                                    Result= [Expression|PartialResult].

get_Subterms( Expression, PartialResult, Result ):- Expression =.. [_, SubExpA],
                                                    get_Subterms(SubExpA,[],PartResA),
                                                    append(PartResA, [Expression|PartialResult], Result),!.

get_Subterms( Expression, PartialResult, Result ):- !,Expression =.. [_, SubExpA, SubExpB],
                                                    get_Subterms(SubExpA,[],PartResA),
                                                    get_Subterms(SubExpB,[],PartResB),
                                                    append(PartResA, PartResB, PartResC),
                                                    append(PartResC, [Expression|PartialResult], Result).

superpose( RuleN, RuleM, CriticalPair):-    rule(RuleN,L1,R1),
                                            rule(RuleM,L2, R2),
                                            print(L1),print('\t->\t'),print(R1),print('\n'),
                                            print(L2),print('\t->\t'),print(R2),print('\n'),
                                            get_Subterms(L1,[],Bits),
                                            match(Bits,L2),
                                            CriticalPair=[L1,R1],
                                            portray_clause(CriticalPair).

match([],_,_,_):-false.

match([Head|Tail],Target):-   Tail=[],
                                    unify(Head,Target).
match([Head|Tail],Target):-   Tail\=[],
                                    unify(Head,Target);
                                    match(Tail,Target).


%match([Head|Tail],R1,Target,R2):-   Tail=[],
%                                    unify_pair(Head,R1,Target,R2).
%match([Head|Tail],R1,Target,R2):-   Tail\=[],
%                                    unify_pair(Head,R1,Target,R2);
%                                    match(Tail,R1,Target,R2).

%unify_pair(L1,R1,L2,R2):-   unify(L1,L2),
%                            print('{'),print(L1),print(','),print(R1),print('}').


% working_directory(_, '/Users/efrenchavezochoa/Desktop/MODELOS/Proloco').
%rule(_,L,_),get_Subterms(L,[],S).
% [filename].
%unify_pair(e+X,X,Y+f,Y).
