% -----------------------------------------------------------------------
%
% Program: A CNF normaliser for propositional logic
% Date:    Febreruary, '93
%
% -----------------------------------------------------------------------

% -----------------------------------------------------------------------
% Description: Given a sentence, $T$, cnf/2 converts $T$ into
%	       clausal form, provided that $T$ is a proposition.. 
%
%	       The logical connectives, together with the Prolog terms 
%	       that represent them, are as follows: 
%
%		1) And				^
%		2) Or				v
%		3) Logical Implication		->
%		4) Logical Equivalence		<->
%		5) Negation			~
%
% -----------------------------------------------------------------------


/*********************************************************************
 *                        Propositional Stuff                        *
 ********************************************************************/

:- op(600, xfy, <->).
:- op(575, xfy, ->).
:- op(550, xfy, ^).
:- op(525, xfy, v).
:- op(500, fy, ~).

%
% object_constant(+P) succeeds whenever +P is an object constant

object_constant(P) :-
    atom(P),
    atom_chars(P,[C|_]), 
    atom_chars(a,[A]), atom_chars(z,[Z]), 
    atom_chars('0',[Z0]), atom_chars('9',[N9]),
    ((C>=A, C=<Z) ; (C>=Z0, C=<N9)),
    \+ C='v'.

/*
 * proposionp(+Term) holds only +Term, a Prolog Term, is a sentence in
 * the propositional logic
 */

proposicionp(P) :- object_constant(P), !.

proposicionp(~P) :- !, proposicionp(P).
proposicionp(P) :-
    P =.. [Op,P1,P2],
    member(Op,[^,v,->,<->]),
    proposicionp(P1),
    proposicionp(P2).

/*********************************************************************
 *                      Term-Rewriting Stuff                         *
 ********************************************************************/

/* 
 * Meta-logical Term-Rewriting Operator 
 */

:- op(700, xfx, :=>).


/* exp_at(+Exp, ?Pos, ?SubExp) 
 * Expression Exp contains SubExp at position Pos. Positions are lists of
 * integers representing tree-coordinates in the syntax-tree of the
 * expression, but in reverse order. Furthermore, coordinate 0
 * represents the function symbol of an expression, thus:
 * exp_at(f(g(2,x),3), [2,1], x) and exp_at(f(g(2,x),3), [0,1], g).
 * The definition from note 349 is extended by defining [] as the
 * position of Exp in Exp.  Fails if Pos is an invalid position in
 * Exp.
 */

exp_at(Var, [], Var) :- var(Var), !.
exp_at(Atomic, [], Atomic) :- atomic(Atomic), !.
exp_at(Exp, [], Exp).

%% SubExp is a Functor
exp_at(Term, [0], Functor) :- functor(Term, Functor, _).

%% SubExp is in Args
exp_at(Term, [Nth | Pos], SubExp) :- 
	Term =.. [_ | Args],
	nth(Nth, NthArg, Args),
	exp_at(NthArg, Pos, SubExp).

rexp_at(Expresion, Pos, SubExpresion) :-
    rexp_at(Expresion, [], Pos, SubExpresion).

/*
 * rexp_at(Exp, PosMid, Pos, SubExp) sginifica que Exp, que aparece en
 * posicion PosMid de un termino mas grande, contiene SubExp, el cual
 * aparece en posicion Pos dentro del termino "mas grande" 
 */
rexp_at(Expresion, Pos, Pos, Expresion).
rexp_at(Expresion, PosMid, Pos, SubExpresion) :-
    \+ atom(Expresion),
    Expresion =.. [SubExpresion|_],
    append(PosMid, [0], Pos).
rexp_at(Expresion, PosMid, Pos, Argi) :-
    \+ atom(Expresion),
    Expresion =.. [_|Args],
    nth(I, Argi, Args),
    append(PosMid, [I], Pos).


/* mexp_at(+Exp, ?Pos, ?SubExp) 
 * Expression Exp contains SubExp at position Pos. Positions are lists of
 * integers representing tree-coordinates in the syntax-tree of the
 * expression, but in reverse order. Furthermore, coordinate 0
 * represents the function symbol of an expression, thus:
 * exp_at(f(g(2,x),3), [2,1], x) and exp_at(f(g(2,x),3), [0,1], g).
 * Fails if Pos is an invalid position in Exp.
 */

mexp_at(Expression, Pos, SubExpression) :-
    mexp_at(Expression, [], Pos, SubExpression).

mexp_at(Expression, Pos, Pos, Expression) :-
    var(Expression), !.
mexp_at(Expression, Pos, Pos, SubExpression) :-
    atom(Expression), !, Expression=SubExpression.
mexp_at(Expression, Pos, Pos, Expression).
mexp_at(Expression, MidPos, Pos, SubExpression) :-
    Expression =.. List,
    nth(Nth, NthItem, List), 
    Pi is Nth-1, append(MidPos, [Pi], NewMidPos),
    mexp_at(NthItem, NewMidPos, Pos, SubExpression).

/* replace(?Term, ?Pos, ?Exp, ?NewExp)
 * ?NewExp is the result of placing term ?Term in expression ?Exp at
 * position ?Pos
 */

replace(Term, [], _, Term).
replace(Term, [0], Exp, NewExp) :- 
	atom(Term), !,
	Exp =.. [_ | Args],
	NewExp =.. [Term | Args].
replace(Term, [Ith|Pos], Exp, NewExp) :-
	nonvar(Exp),
	Exp =.. [Functor | Args],
	nth(Ith, IthExp, Args),
	replace(Term, Pos, IthExp, NewIthExp),
	replace_list(NewIthExp, Ith, Args, NewArgs),
	NewExp =.. [Functor | NewArgs].

/* replace_list(?NewTerm, ?Nth, ?List, ?NewList)
 * ?NewList is as ?List except for the ?Nth item which is now ?NewTerm 
 */

replace_list(Term, Nth, List, NewList) :-
	replace_list(1, Term, Nth, List, NewList).

replace_list(Nth, T, Nth, [_|Rest], [T|Rest]).
replace_list(Ith, T, Nth, [First|Rest], [First|NRest]) :-
	Jth is Ith + 1,
	replace_list(Jth, T, Nth, Rest, NRest).


/* replace_all(?NewTerm, ?OldTerm, Exp, NewExp)
 *  
 * ?NewExp is as ?Exp except that every occurrence of ?OldTerm in ?Exp
 * has been replaced by ?NewTerm
 */

replace_all(NewTerm, OldTerm, Exp, NewExp) :-
	findall(Pos, exp_at(Exp, Pos, OldTerm), PosList),
	replace_all_list(PosList, NewTerm, Exp, NewExp).

/* replace_all_list(?PosList, ?NewTerm, ?Exp, ?NewExp)
 * 
 * Pairwise extension to replace/4 
 */

replace_all_list([], _, Exp, Exp).
replace_all_list([Pos|PosList], Term, Exp, NewExp) :-
	replace(Term, Pos, Exp, ExpSoFar),
	replace_all_list(PosList, Term, ExpSoFar, NewExp).


/*********************************************************************
 *                             Interpreter                           *
 ********************************************************************/

applicable([]) :- true.
applicable([Clause]) :- call(Clause).
applicable([Clause|Clauses]) :- call(Clause), applicable(Clauses).


/*********************************************************************
 *                            Main Clauses                           *
 ********************************************************************/

/* 
 * cnf(+S, ?C)
 * ?C is the clausal form of ?S
 */

cnf(S,C) :-  
    exp_at(S,Pos,SubExp),
    rr(_,_,SubExp:=>NewExp,_),
    replace(NewExp,Pos,S,NewS),!,
    cnf(NewS,C).
cnf(S,S).

displayconjuncts(C) :-
    C=..[^,C1,C2], !,
    displayconjuncts(C1),
    displayconjuncts(C2).
displayconjuncts(C) :- display(C),nl.

/*********************************************************************
 *                         List Predicates                           *
 ********************************************************************/

/* member(?Item, ?List)
 *
 * succeeds whenever ?Item appears in ?List
 */

member(X, [X|_]).
member(X, [_|T]) :- member(X, T).


/* nth(?Nth, ?Item, +List)
 * ?Item is the ?Nth element, i.e. occupies the ?Nth position of +List 
 */

nth(Nth, Item, List) :- nth(Nth, Item, List, 1).

%% nth/4
nth(Current, Item, [Item|_], Current).
nth(Nth, Item, [_|List], Current) :- 
	Next is Current + 1,
	nth(Nth, Item, List, Next).

/* append(?L1, ?L2, ?L)
 *
 * L is the result of concatenating L1 and L2, in that order
 */

append([], L, L).
append([H|T], L2, [H|L]) :- append(T, L2, L).

/***********************************************************************
 *                         KNOWLEDGE BASE                              *
 **********************************************************************/

%% Preprocessing

rr(pre, double_implication, (A <-> B) :=> (A -> B) ^ (B -> A), []).
rr(pre, implication, A -> B :=> ~A v B, []).

%% Conjunctive Normal Form

rr(cnf, not, ~ ~ A :=> A, []).
rr(cnf, deMorgan1, ~(A v B) :=> ~A ^ ~B, []).
rr(cnf, deMorgan2, ~(A ^ B) :=> ~A v ~B, []).
rr(cnf, or_distribution_left, A v (B ^ C) :=> (A v B) ^ (A v C), []).
rr(cnf, or_distribution_right, (B ^ C) v A :=> (B v A) ^ (C v A), []).

